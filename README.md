HASHCODE 2018 by Symbol-IT
==========================


Quick start
-----------

Run :
```bash
$> npm start data/a_example.in a_example.out
```
for i in "a_example" "b_should_be_easy" "c_no_hurry" "d_metropolis" "e_high_bonus"; do echo "$i"; npm start "data/${i}.in" "${i}.out"; done
