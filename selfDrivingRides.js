#!/usr/bin/env node

const fs = require('fs');
const readLine = require('readline')
const _ = require('lodash')

const inputFileName = process.argv[2]
const outputFileName = process.argv[3]

let city
const rides = []

let currentLine = 0

readCity = (line) => {
    [rows,cols,fleetSize, nbRides, bonus, steps] = line.split(' ')
    return {
        rows,
        cols,
        fleetSize,
        nbRides,
        bonus,
        steps
    }
}

readRide = (line) => {
    [row, col, x, y, startStep, finishStep] = line.split(' ')
    return {
        row, 
        col, 
        x, 
        y, 
        startStep, 
        finishStep 
    }

}
const lineReader = readLine.createInterface({
    input: fs.createReadStream(inputFileName)
});
  
lineReader.on('line', function (line) {
    if (currentLine === 0) {
        city = readCity(line)
    } else {
        rides.push(readRide(line))
    }
    currentLine++
});

lineReader.on('close',  () => {
    startSimulation(city, rides)
})

initMapRideVehicle = () => {
    return rides.map(() => -1)
}

function affecter(vehicle, goNoGo, mapRideVehicules) {
    //goNoGo[vehicle.index].forEach( (goNoGoRide, rideIndex) =>
    for (rideIndex=0; rideIndex < goNoGo[vehicle.index].length; rideIndex++) {
        goNoGoRide = goNoGo[vehicle.index][rideIndex]

        if (goNoGoRide == true && mapRideVehicules[rideIndex] === -1) {
            mapRideVehicules[rideIndex] = vehicle.index
            vehicle.distanceAParcourir = rides[rideIndex].longueur
            vehicle.distanceParcourue = 0
            break
        }
    }
}

function avancer(vehicle) {
    vehicle.distanceParcourue++
}

function isDispo(vehicle) {
    return (vehicle.distanceAParcourir - vehicle.distanceParcourue) === 0
}

startSimulation = (city, rides) => {

    rides.forEach(ride => {
        ride.longueur = calculRideDistance(ride)
    })

    const mapRideVehicules = initMapRideVehicle();
    step0();

    for (let currentStep = 0; currentStep <= city.steps; currentStep++) {
        const startPointTime = fillStartPointTime();
        const goNoGo = fillGoNoGo(currentStep, startPointTime);

        vehicles.forEach(vehicle => {
            if (isDispo(vehicle)) {
                affecter(vehicle, goNoGo, mapRideVehicules)
            } else {
                avancer(vehicle)
            }
        })

        //console.log(mapRideVehicules)
    }

    results = vehicles.map(() => [])

    mapRideVehicules.forEach( (indexVehicle, indexRide) => {
        if (indexVehicle != -1)
            results[indexVehicle].push(indexRide)
    })

    //console.log("results = ", results)

    writeOuput(results)
}

writeOuput = (results) => {
    fs.writeFile(outputFileName, "", (err) => {
        if (err) throw err;
    })

    results.forEach( (result) => {
        line = result.length + " " + result.join(" ")
        fs.appendFileSync(outputFileName, line + "\n")
    })
}

calculRideDistance = (ride) => {
    return Math.abs(ride.row - ride.x) + Math.abs(ride.col - ride.y) 
}

calculCarRideDistance = (car, ride) => {
    return Math.abs(car.posX - ride.row) + Math.abs(car.posY - ride.col)
}

goOrNotGo = (earlyStep, currentStep, carRideDistance, rideDistance, maxStep) => {
    let avanceRetardOuAheure = earlyStep - currentStep - carRideDistance;
    return (avanceRetardOuAheure + rideDistance) < (maxStep - currentStep)
}

class Vehicle {
    constructor(index, posX, posY, distanceParcourue, distanceAParcourir) {
        this.posX = posX;
        this.posY = posY;
        this.distanceParcourue = distanceParcourue;
        this.distanceAParcourir = distanceParcourue;
        this.index = index
    }
}

//////////////////////////////////////////////
// Step 0 : Initialisation des voitures

const vehicles = [];

function step0() {
    for (let i = 0; i < city.fleetSize; i++) {
        vehicles.push(new Vehicle(i, 0, 0, 0, 0));
    }
    //console.log(vehicles);
}

function fillStartPointTime() {
    startPointTime = [];
    vehicles.forEach(vehicle => {
        const vTab = [];
        startPointTime.push(vTab);
        rides.forEach(ride => {
            vTab.push(calculCarRideDistance(vehicle, ride));
        })
    })
    //console.log(startPointTime);
    return startPointTime;
}


function fillGoNoGo(currentStep, startPointTime) {
    const goNoGo = [];
    vehicles.forEach((vehicle, vehicleIndex) => {
        const vTab = [];
        goNoGo.push(vTab);
        rides.forEach((ride, rideIndex) => {
            vTab.push(goOrNotGo(ride.startStep, currentStep, startPointTime[vehicleIndex][rideIndex], ride.longueur, city.steps));
        })
    })
    //console.log(goNoGo);
    return goNoGo
}

